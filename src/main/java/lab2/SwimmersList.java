package lab2;

import lab2.iteration.DefaultVectorIterator;
import lab2.iteration.internal.InternalAgeIterator;
import lab2.iteration.ReverseIterator;

import java.util.*;
import lab2.iteration.internal.InternalIterator;

public class SwimmersList
{
    private Vector<Swimmer> slist= new Vector<Swimmer>();

    public void addSwimmer( Swimmer swimmer ){
        slist.addElement(swimmer);
    }
    
    public Vector<Swimmer> getVector(){
        return slist;
    }

    public Iterator<Swimmer> getDefaultIterator(){
        return new DefaultVectorIterator<Swimmer>(slist).getIterator();
    }

    public Iterator<Swimmer> getReverseIterator(){
        return new ReverseIterator<Swimmer>(slist).getIterator();
    }

    public InternalIterator<Swimmer> getInternalIterator(){
      return new InternalAgeIterator<Swimmer>(slist);
    }
}
