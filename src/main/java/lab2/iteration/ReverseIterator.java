package lab2.iteration;

import java.util.*;

public class ReverseIterator<T> implements VectorIterator<T> {
    private Vector<T> vect;

    public ReverseIterator(Vector<T> v){
        vect=v;
    }
    
    public Iterator<T> getIterator() {

		return new Iterator<T>(){
		   int count = vect.size()-1;
			public boolean hasNext() {
				return count > -1;
			}
			public T next() {
				synchronized (vect) {
					if (count > -1) {
						return vect.elementAt(count--);
					}
				}
				throw new NoSuchElementException("No elem.");
			}
		};
    }
}
