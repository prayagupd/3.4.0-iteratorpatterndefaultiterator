package lab2.iteration;

import java.util.Enumeration;
import java.util.Iterator;

/**
 * Created by 984493
 * on 4/3/2015.
 */

public interface VectorIterator<T> {
    public Iterator<T> getIterator();
}
