package lab2.iteration.internal;

import java.util.Iterator;

/**
 * Created by prayagupd
 * on 4/3/15.
 */

public interface InternalIterator<T> {
  public Iterator<T> iterator(Functor<T> functor);
}
