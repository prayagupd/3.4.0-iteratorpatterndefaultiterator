package lab2.iteration.internal;

import java.util.Vector;
import lab2.Swimmer;

public interface Functor<T>{
    public void apply(T obj);
    public Vector<T> value();
}
