package lab2.iteration.internal;

import java.util.*;

public class InternalAgeIterator<T> implements InternalIterator<T> {
    private Vector<T> vect;
    
    public InternalAgeIterator(Vector<T> v){
        vect=v;
    }

    @Override
    public Iterator<T> iterator(Functor<T> functor) {
          for ( T s : vect){
              functor.apply(s);
          }
          vect = functor.value();
          return new Iterator<T>() {
            int count = 0;
            @Override public boolean hasNext() {
              return count < vect.size();
            }

            @Override public T next() {
              return vect.get(count++);
            }
          };
      }
}
