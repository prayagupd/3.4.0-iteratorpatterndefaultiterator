package lab2.iteration;

import java.util.*;

public class DefaultVectorIterator<T> implements VectorIterator<T> {
    private Vector<T> vector;
    
    public DefaultVectorIterator(Vector<T> v){
        vector =v;
    }
    
    public Iterator<T> getIterator() {
		return new Iterator<T>() {
			int count = 0;

			public boolean hasNext() {
				return count < vector.size();
			}

			public T next() {
				synchronized (vector) {
					if (count < vector.size()) {
						return vector.elementAt(count++);
					}
				}
				throw new NoSuchElementException("No elem.");
			}
		};
    }
    
}
