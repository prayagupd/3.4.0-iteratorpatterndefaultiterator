package lab2.iteration;

import java.util.Vector;
import lab2.Swimmer;

import lab2.iteration.internal.Functor;

public class SwimmerFunctor implements Functor<Swimmer> {
    Vector<Swimmer> vector;
    
    public SwimmerFunctor(){
      this.vector = new Vector<Swimmer>();
    }
    public void apply(Swimmer swimmer){
      if (swimmer.getAge()>=12)
             vector.add(swimmer);
    }

  @Override public Vector<Swimmer> value() {
    return vector;
  }
}
